import 'dart:js';

import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    initialRoute: '/',
    routes: {
      '/': (context) => const FirstScreen(),
      '/second': (context) => const SecondScreen(),
      '/thir': (context) => const ThirScreen()
    },
  ));
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('First Screen'),
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 8,
            ),
            ElevatedButton(
              child: const Text('Launch SecondSceen'),
              onPressed: () {
                Navigator.pushNamed(context, '/second');
              },
            ),
            SizedBox(
              height: 8,
            ),
            ElevatedButton(
              child: const Text('Launch ThirSceen'),
              onPressed: () {
                Navigator.pushNamed(context, '/thir');
              },
            ),
          ],
        )
      ),
    );
  }
}

class ThirScreen extends StatelessWidget {
  const ThirScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Thir Screen'),
      ),
      body: Center(
          child: Column(
        children: [
          SizedBox(
            height: 8,
          ),
          ElevatedButton(
            child: const Text('Go To Second Screen'),
            onPressed: () {
              Navigator.pushNamed(context, '/second');
            },
          ),
          SizedBox(
            height: 8,
          ),
          ElevatedButton(
            child: const Text('Go Back Sceen'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      )),
    );
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second Screen'),
      ),
      body: Center(
        child: ElevatedButton(
          child: const Text('Go Back Sceen'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
